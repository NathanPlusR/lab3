package dawson;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    @Test
    public void echoTest()
    {
        org.junit.Assert.assertEquals("Testing echo method's output", 5, App.echo(5));
    }

    @Test
    public void oneMoreTest()
    {
        org.junit.Assert.assertEquals("Testing oneMore method's output", 6, App.oneMore(5));
    }
}
